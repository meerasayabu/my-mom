//
//  MOMCollectionViewCell.swift
//  MyMOM
//
//  Created by Meera Sayabu on 27/04/19.
//  Copyright © 2019 Meera Sayabu. All rights reserved.
//

import UIKit

class MOMCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ownerNameLabel: UILabel!
    @IBOutlet weak var createdDateLabel: UILabel!
    @IBOutlet weak var priorityLabel: UILabel!
    @IBOutlet weak var taskStatusIconImageView: UIImageView!
    @IBOutlet weak var noteTextView: UITextView!
    
    func configureCell(note: Note) {
        titleLabel.text = note.title
        ownerNameLabel.text = note.name
        createdDateLabel.text = note.date.toString(with: dateFormat)
        priorityLabel.text = note.priority.indentifier
        taskStatusIconImageView.image = note.isTaskCompleted ? UIImage(named: "completed-task") : UIImage(named: "not completed")
        noteTextView.text = note.note
        noteTextView.isEditable = false
    }
}
