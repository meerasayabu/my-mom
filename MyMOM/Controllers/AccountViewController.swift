//
//  AccountViewController.swift
//  MyMOM
//
//  Created by Meera Sayabu on 29/04/19.
//  Copyright © 2019 Meera Sayabu. All rights reserved.
//

import UIKit
import FirebaseAuth
import MaterialComponents

class AccountViewController: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var placeHolderLabel: UILabel!
    
    //MARK: Variables
    private var accountViewModel: AccountViewable = AccountViewModel()
    private var message = MDCSnackbarMessage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        accountViewModel.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        nameLabel.isHidden = !accountViewModel.isUserLoggedIn
        emailLabel.isHidden = !accountViewModel.isUserLoggedIn
        logoutButton.isHidden = !accountViewModel.isUserLoggedIn
        placeHolderLabel.isHidden = accountViewModel.isUserLoggedIn
        
        nameLabel.text = accountViewModel.userName
        emailLabel.text = accountViewModel.userEmail
    }
    
    func setupViews() {
        emailLabel.layer.cornerRadius = 10
        nameLabel.layer.cornerRadius = 10
        logoutButton.layer.cornerRadius = 10
    }

    @IBAction func logout(_ sender: UIButton) {
        accountViewModel.signOut()
    }
}

extension AccountViewController: AccountViewModelDelegate {
    func loggedOut() {
        tabBarController?.selectedIndex = 0
    }
    
    func logoutFailedWith(error: Error) {
        message.text = "Login Failed. Error\(error.localizedDescription)"
        MDCSnackbarManager.show(message)
    }
}

extension UIViewController {
    func navigateToLoginViewController() {
        guard Auth.auth().currentUser?.refreshToken == nil else {
            return
        }
        navigationController?.resetAndPushLoginViewController()
    }
}
