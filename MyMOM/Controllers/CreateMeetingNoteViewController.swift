//
//  CreateMeetingNoteViewController.swift
//  MyMOM
//
//  Created by Meera Sayabu on 27/04/19.
//  Copyright © 2019 Meera Sayabu. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import MaterialComponents
import NVActivityIndicatorView
import SnapKit

let dateFormat = "dd MMM yyyy"

class CreateMeetingNoteViewController: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var titleTextField: MOMTextField!
    @IBOutlet weak var dateTextField: MOMTextField!
    @IBOutlet weak var noteTextView: UITextView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var highButton: UIButton!
    @IBOutlet weak var mediumButton: UIButton!
    @IBOutlet weak var lowButton: UIButton!
    @IBOutlet weak var taskSwitch: UISwitch!
    
    //MARK: Variables
    private var databaseReference: DatabaseReference!
    private var createNoteViewModel: CreateNoteViewable = CreateNoteViewModel(homeViewModel: HomeViewModel())
    var editNote: Note?
    private var message = MDCSnackbarMessage()
    private let activityData = ActivityData(type: .ballSpinFadeLoader)
    private let datePickerView = UIDatePicker()
    private var selectedDate: Date?

    override func viewDidLoad() {
        super.viewDidLoad()
        title = Titles.note
        setup()
    }
    
    func setup() {
        configureView()
        setConformance()
        addGesture()
        setupEditNote()
    }
    
    private func setConformance() {
        titleTextField.delegate = self
        dateTextField.delegate = self
        createNoteViewModel.delegate = self
    }
    
    private func configureView() {
        saveButton.layer.cornerRadius = 5
        lowButton.layer.cornerRadius = 5
        mediumButton.layer.cornerRadius = 5
        highButton.layer.cornerRadius = 5
        // set date validation. User can select till today
        datePickerView.maximumDate = Date()
        taskSwitch.isOn = false
    }
    
    private func setupEditNote() {
        guard let note = editNote else {
            return
        }
        titleTextField.text = note.title
        dateTextField.text = note.date.toString(with: dateFormat)
        noteTextView.text = note.note
        createNoteViewModel.isCompleted = note.isTaskCompleted
        taskSwitch.isOn = note.isTaskCompleted
        createNoteViewModel.priority = note.priority
        
        switch note.priority {
        case .low:
            lowButton.backgroundColor = UIColor.lightGray
        case .medium:
            mediumButton.backgroundColor = UIColor.lightGray
        case .high:
            highButton.backgroundColor = UIColor.lightGray
        default:
            break
        }
    }
    
    private func addGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc private func hideKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func setStatusOfTask(_ sender: UISwitch) {
        createNoteViewModel.isCompleted = sender.isOn
    }

    @IBAction func save(_ sender: UIButton) {
        let validation = createNoteViewModel.isValidateTextInFields(titleTextField: titleTextField, dateTextField: dateTextField, noteTextView: noteTextView)
        if selectedDate == nil {
            selectedDate = dateTextField.text?.toDate(format: dateFormat)
        }
        
        guard validation.0 else {
            message.text = validation.1
            MDCSnackbarManager.show(message)
            return
        }
        
        print(editNote?.key ?? "")
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
        createNoteViewModel.saveNote(title: titleTextField.text!, note: noteTextView.text, date: (dateTextField.text!.toDate(format: dateFormat)?.timeIntervalSince1970)!, isUpdate: editNote == nil ? false : true, noteKey: editNote?.key)
    }
    
    @IBAction func setPriority(_ sender: UIButton) {
        sender.backgroundColor = .lightGray
        switch sender.tag {
        case 101:
            createNoteViewModel.priority = .low
            highButton.backgroundColor = .white
            mediumButton.backgroundColor = .white
        case 102:
            createNoteViewModel.priority = .medium
            lowButton.backgroundColor = .white
            highButton.backgroundColor = .white
        default:
            createNoteViewModel.priority = .high
            lowButton.backgroundColor = .white
            mediumButton.backgroundColor = .white
        }
    }
    
    //MARK: IBActios
    @IBAction func setDate(_ sender: MOMTextField) {
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControl.Event.valueChanged)
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        selectedDate = sender.date
        dateTextField.text = selectedDate?.toString(with: dateFormat)
    }
}

//MARK: CreateViewModelDelegate
extension CreateMeetingNoteViewController: CreateViewModelDelegate {
    func didNotesSaved() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
        message.text = "Note saved successfully"
        MDCSnackbarManager.show(message)
        navigationController?.popViewController(animated: true)
    }
    
    func didSavingFailed(error: Error?) {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
        message.text = "Failed to save the notes. Reason: \(error!.localizedDescription)"
        MDCSnackbarManager.show(message)
    }
}

//MARK: UITextFieldDelegate
extension CreateMeetingNoteViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return false
    }
}
