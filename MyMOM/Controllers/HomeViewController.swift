//
//  ViewController.swift
//  MyMOM
//
//  Created by Meera Sayabu on 27/04/19.
//  Copyright © 2019 Meera Sayabu. All rights reserved.
//

import UIKit
import FirebaseAuth
import NVActivityIndicatorView

class HomeViewController: UIViewController {
    @IBOutlet weak var momCollectionView: UICollectionView!
    @IBOutlet weak var bannerTitleLabel: UILabel!
    @IBOutlet weak var bannerNameLabel: UILabel!
    @IBOutlet weak var bannerDateLabel: UILabel!
    @IBOutlet weak var bannerPriorityLabel: UILabel!
    @IBOutlet weak var loadingPlacegolderLabel: UILabel!
    
    //MARK: Variables
    private var homeViewModel: HomeViewable = HomeViewModel()
    private let activityData = ActivityData(type: .ballSpinFadeLoader)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = Titles.myMeetings
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        navigateToLoginViewController()
    }
    
    private func setup() {
        setupViews()
        setConformance()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
        homeViewModel.notifyNotesChanges()
        setupHomeNoteBanner()
        addBarButtons()
    }
    
    private func addBarButtons() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Sort", style: .plain, target: self, action: #selector(sort))
    }
    
    @objc private func sort() {
        navigationController?.presentFilterViewController(delegate: self)
    }
    
    private func setConformance() {
        momCollectionView.delegate = self
        momCollectionView.dataSource = self
        homeViewModel.delegate = self
    }
    
    private func setupViews() {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 2
        layout.minimumInteritemSpacing = 1
        momCollectionView.collectionViewLayout = layout
    }
    
    @IBAction func createNote(_ sender: UIButton) {
        navigationController?.pushCreateNoteViewController()
    }
}

extension HomeViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        homeViewModel.search(query: searchText)
    }
}

extension HomeViewController: HomeViewModelDelegate {
    func didNoteUpdateNotified(result: NoteResult) {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
        switch result {
        case .empty:
            loadingPlacegolderLabel.text = "Create your very first meeting"
            momCollectionView.isHidden = true
        case .nonEmpty:
            loadingPlacegolderLabel.isHidden = true
            momCollectionView.isHidden = false
            momCollectionView.reloadData()
        }
    }
    
    func setupHomeNoteBanner() {
        bannerNameLabel.text = Auth.auth().currentUser?.displayName
        bannerTitleLabel.text = "Meeting Title"
        bannerPriorityLabel.text = "Priority of meeting notes action items"
        bannerDateLabel.text = Date().toString(with: dateFormat)
    }
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "momCell", for: indexPath) as! MOMCollectionViewCell
        cell.configureCell(note: homeViewModel.notes![indexPath.row])
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return homeViewModel.notes?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: CGFloat((collectionView.frame.size.width / 2)-1), height: CGFloat(212))
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        navigationController?.pushCreateNoteViewController(note: homeViewModel.notes?[indexPath.row])
    }
}

extension HomeViewController: SortAndFilterDelegate {
    func selected(type: FilterType) {
        homeViewModel.sort(type: type)
    }
}
