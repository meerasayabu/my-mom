//
//  SortAndFilterViewController.swift
//  MyMOM
//
//  Created by Meera Sayabu on 28/04/19.
//  Copyright © 2019 Meera Sayabu. All rights reserved.
//

import UIKit

enum FilterType: String {
    case date = "date"
    case priority = "priority"
    case status = "isTaskCompleted"
    case none
}

protocol SortAndFilterDelegate: NSObjectProtocol {
    func selected(type: FilterType)
}

class SortAndFilterViewController: UIViewController {

    @IBOutlet weak var priorityButton: UIButton!
    @IBOutlet weak var statusButton: UIButton!
    @IBOutlet weak var dateButton: UIButton!
    @IBOutlet weak var iconImageView: UIImageView!
    
    weak var delegate: SortAndFilterDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = Titles.sortBy
        setupView()
    }
    
    private func setupView() {
        priorityButton.layer.cornerRadius = 10
        dateButton.layer.cornerRadius = 10
        statusButton.layer.cornerRadius = 10
    }

    @IBAction func close(_ sender: UIButton) {
        delegate?.selected(type: .none)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dateSelected(_ sender: Any) {
        delegate?.selected(type: .date)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func prioritySelected(_ sender: Any) {
        delegate?.selected(type: .priority)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func starusSelected(_ sender: Any) {
        delegate?.selected(type: .status)
        dismiss(animated: true, completion: nil)
    }
    
}
