//
//  RegistrationViewController.swift
//  MyMOM
//
//  Created by Meera Sayabu on 27/04/19.
//  Copyright © 2019 Meera Sayabu. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import MaterialComponents
import NVActivityIndicatorView

class RegistrationViewController: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var emailTextField: MOMTextField!
    @IBOutlet weak var passwordTextField: MOMTextField!
    @IBOutlet weak var nameTextField: MOMTextField!
    @IBOutlet weak var confirmPasswordTexxtField: MOMTextField!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    

    //MARK: Variables
    private var registrationViewModel: RegistrationViewable = RegistrationViewModel()
    private let activityData = ActivityData(type: .ballSpinFadeLoader)
    
    //MARK: Life cycle metods
    override func viewDidLoad() {
        super.viewDidLoad()
        title = Titles.myMeetings
        setup()
        
        if let _ = Auth.auth().currentUser {
            navigationController?.pushHomeViewController()
        }
    }
    
    private func setup() {
        configureView()
        addGesture()
        setConformance()
    }
    
    private func setConformance() {
        emailTextField.delegate = self
        passwordTextField.delegate = self
        confirmPasswordTexxtField.delegate = self
        nameTextField.delegate = self
        registrationViewModel.delegate = self
    }
   
    private func configureView() {
        loginButton.layer.cornerRadius = 5
        signUpButton.layer.cornerRadius = 5
    }
    
    private func addGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc private func hideKeyboard() {
        view.endEditing(true)
    }
    
    //MARK: Action methods
    @IBAction func register(_ sender: UIButton) {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
        //Registeration
        registrationViewModel.registerUser(email: emailTextField.text, password: passwordTextField.text, confirmPassword: confirmPasswordTexxtField.text, name: nameTextField.text)
    }
    
    
    @IBAction func navigateToLoginScreen(_ sender: UIButton) {
        navigationController?.pushLoginViewController()
    }
}

//MARK: RegistrationViewModelDelegate
extension RegistrationViewController: RegistrationViewModelDelegate {
    func invalidInput(type: InvalidInputType) {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
        let message = MDCSnackbarMessage()
        message.text = type.errorMessage
        MDCSnackbarManager.show(message)
    }
    
    func registrationResult(resultType: ResultType, error: Error?) {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
        switch resultType {
        case .success:
            resetFields()
            navigationController?.pushLoginViewController()
        case .failure:
            let message = MDCSnackbarMessage()
            message.text = registrationViewModel.registrationFailedMessage(error: error)
            MDCSnackbarManager.show(message)
        }
    }
    
    func resetFields() {
        nameTextField.text = nil
        emailTextField.text = nil
        passwordTextField.text = nil
        confirmPasswordTexxtField.text = nil
    }
}

extension RegistrationViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return false
    }
}
