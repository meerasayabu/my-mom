//
//  LoginViewController.swift
//  MyMOM
//
//  Created by Meera Sayabu on 27/04/19.
//  Copyright © 2019 Meera Sayabu. All rights reserved.
//

import UIKit
import Firebase
import MaterialComponents
import NVActivityIndicatorView

class LoginViewController: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var passwordTexxtField: MOMTextField!
    @IBOutlet weak var emailTextField: MOMTextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    
    //MARK: Variables
    private var loginViewModel: LoginViewable = LoginViewModel(registrationViewModel: RegistrationViewModel())
    private var message = MDCSnackbarMessage()
    private let activityData = ActivityData(type: .ballSpinFadeLoader)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = Titles.login
        loginViewModel.delegate = self
        emailTextField.delegate = self
        passwordTexxtField.delegate = self
        configureView()
        addGesture()
    }
    
    private func configureView() {
        loginButton.layer.cornerRadius = 5
        registerButton.layer.cornerRadius = 5
    }
    
    private func addGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc private func hideKeyboard() {
        view.endEditing(true)
    }

    @IBAction func login(_ sender: UIButton) {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
        loginViewModel.login(email: emailTextField.text, password: passwordTexxtField.text)
    }
    
    @IBAction func register(_ sender: UIButton) {
        navigationController?.pushRegisterViewController()
    }
}

extension LoginViewController: LoginDelegate {
    func didLoginSuccessful(result: AuthDataResult?) {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
        if let name = result?.user.displayName {
            message.text = "WELCOME \(name)"
            MDCSnackbarManager.show(message)
        }
        navigationController?.pushHomeViewController()
    }
    
    func didLoginFailed(error: Error?) {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
        if let err = error {
            message.text = "Login failed, \(err.localizedDescription)"
        } else {
            message.text = "Login failed"
        }
        MDCSnackbarManager.show(message)
    }
    
    func invalidInput(type: InvalidInputType) {
        message.text = type.errorMessage
        MDCSnackbarManager.show(message)
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return false
    }
}
