//
//  Double+Extension.swift
//  MyMOM
//
//  Created by Meera Sayabu on 28/04/19.
//  Copyright © 2019 Meera Sayabu. All rights reserved.
//

import Foundation

extension Double {
    func toDate() -> Date {
        return Date(timeIntervalSince1970: (self / 1000.0))
    }
    
    public func toString(with format:String) -> String? {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.timeZone = TimeZone(abbreviation: "IST") //TimeZone.ReferenceType.local //TimeZone.current
        return formatter.string(from: self.toDate())
    }
}
