//
//  Bool+Extension.swift
//  MyMOM
//
//  Created by Meera Sayabu on 28/04/19.
//  Copyright © 2019 Meera Sayabu. All rights reserved.
//

import Foundation

extension Bool {
    var intValue: Int {
        return self ? 1 : 0
    }
}
