//
//  Date+Extension.swift
//  MyMOM
//
//  Created by Meera Sayabu on 28/04/19.
//  Copyright © 2019 Meera Sayabu. All rights reserved.
//

import Foundation


extension Date {
    public var millisecondsSince1970: Double {
        return Double((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    public func toString(with format:String) -> String? {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.timeZone = TimeZone(abbreviation: "IST") //TimeZone.ReferenceType.local //TimeZone.current
        return formatter.string(from: self)
    }
}
