//
//  ViewController+Extension.swift
//  MyMOM
//
//  Created by Meera Sayabu on 27/04/19.
//  Copyright © 2019 Meera Sayabu. All rights reserved.
//

import Foundation
import UIKit

// Defining view controller titles
extension UIViewController {
    public struct Titles {
        public static let welcome = "Welcome"
        public static let login = "Login"
        public static let myMOM = "My MOM"
        public static let note = "Note"
        public static let myMeetings = "My Meetings"
        public static let sortBy = "Sort By"
    }
}
