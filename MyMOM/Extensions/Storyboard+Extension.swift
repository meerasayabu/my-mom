//
//  Storyboard+Extension.swift
//  MyMOM
//
//  Created by Meera Sayabu on 27/04/19.
//  Copyright © 2019 Meera Sayabu. All rights reserved.
//

import Foundation
import UIKit

struct StoryboardName {
    static let main = "Main"
}

protocol StoryboardIdentifiable {
    static var storyboardIdentifier: String { get }
}

extension StoryboardIdentifiable where Self: UIViewController {
    static var storyboardIdentifier: String {
        return String(describing: self)
    }
}

extension UIViewController: StoryboardIdentifiable { }

extension UINavigationController {
    
    override open var storyboard: UIStoryboard {
        return UIStoryboard(name: StoryboardName.main, bundle: Bundle.main)
    }
    
    func pushLoginViewController() {
        let vc = storyboard.instantiateViewController(withIdentifier: LoginViewController.storyboardIdentifier) as! LoginViewController
        pushViewController(vc, animated: true)
    }
    
    func resetAndPushLoginViewController() {
        let vc = storyboard.instantiateViewController(withIdentifier: LoginViewController.storyboardIdentifier) as! LoginViewController
        setViewControllers([vc], animated: true)
    }
    
    func pushHomeViewController() {
        let vc = storyboard.instantiateViewController(withIdentifier: HomeViewController.storyboardIdentifier) as! HomeViewController
        setViewControllers([vc], animated: true)
    }
    
    func pushRegisterViewController() {
        let vc = storyboard.instantiateViewController(withIdentifier: RegistrationViewController.storyboardIdentifier) as! RegistrationViewController
        setViewControllers([vc], animated: true)
    }
    
    func pushCreateNoteViewController(note: Note? = nil) {
        let vc = storyboard.instantiateViewController(withIdentifier: CreateMeetingNoteViewController.storyboardIdentifier) as! CreateMeetingNoteViewController
        vc.editNote = note
        pushViewController(vc, animated: true)
    }
    
    func presentFilterViewController(delegate: HomeViewController) {
        let vc = storyboard.instantiateViewController(withIdentifier: SortAndFilterViewController.storyboardIdentifier) as! SortAndFilterViewController
        vc.delegate = delegate
        present(vc, animated: true, completion: nil)
    }
}
