//
//  LoginViewModel.swift
//  MyMOM
//
//  Created by Meera Sayabu on 27/04/19.
//  Copyright © 2019 Meera Sayabu. All rights reserved.
//

import Foundation
import FirebaseAuth

protocol LoginDelegate: NSObjectProtocol {
    func didLoginFailed(error: Error?)
    func didLoginSuccessful(result: AuthDataResult?)
    func invalidInput(type:InvalidInputType)
}

protocol LoginViewable {
    var delegate: LoginDelegate? { set get }
    func login(email: String?, password: String?)
}

class LoginViewModel: LoginViewable {
    
    private var registrationViewModel: RegistrationViewable
    weak var delegate: LoginDelegate?
    
    init(registrationViewModel: RegistrationViewable) {
        // Initialize dependencies
        self.registrationViewModel = registrationViewModel
    }
    
    func login(email: String?, password: String?) {
        guard registrationViewModel.isValidEmail(email: email) else {
            delegate?.invalidInput(type: .email)
            return
        }
        
        guard let pwd = password else {
            delegate?.invalidInput(type: .password)
            return
        }
        
        Auth.auth().signIn(withEmail: email!, password: pwd) {[weak self] (result, error) in
            guard let strongSelf = self else { return }
            if error == nil {
                strongSelf.delegate?.didLoginSuccessful(result: result)
            } else {
                strongSelf.delegate?.didLoginFailed(error: error)
            }
        }
    }
}
