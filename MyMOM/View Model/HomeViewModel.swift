//
//  HomeViewModel.swift
//  MyMOM
//
//  Created by Meera Sayabu on 27/04/19.
//  Copyright © 2019 Meera Sayabu. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseAuth

enum NoteResult {
    case empty
    case nonEmpty
}

protocol HomeViewModelDelegate: NSObjectProtocol {
    func didNoteUpdateNotified(result: NoteResult)
}

protocol HomeViewable {
    var delegate: HomeViewModelDelegate? { set get }
    var notes: [Note]? { get }
    func notifyNotesChanges()
    func search(query: String)
    func sort(type: FilterType)
}

class HomeViewModel: HomeViewable {
    weak var delegate: HomeViewModelDelegate?
    var notes: [Note]?
    private var databaseReference = Database.database().reference()
    
    init() {
        // Initialize dependencies
    }
    
    func notifyNotesChanges() {
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        databaseReference.child("users/\(uid)/notes").observe(.value) {[weak self] (dataSnapshot) in
            guard let strongSelf = self else { return }
            strongSelf.parseToNote(dataSnapshot: dataSnapshot)
            
        }
    }
    
    func search(query: String) {
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let sort = databaseReference.child("users/\(uid)/notes").queryOrdered(byChild: "title").queryStarting(atValue: query)
        sort.observe(.value) {[weak self] (dataSnapshot) in
            guard let strongSelf = self else { return }
            strongSelf.parseToNote(dataSnapshot: dataSnapshot)
        }
    }
    
    func parseToNote(dataSnapshot: DataSnapshot) {
        guard dataSnapshot.childrenCount > 0 else {
            print("Notes yet to create")
            delegate?.didNoteUpdateNotified(result: .empty)
            return
        }
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject:toJSon(dataSnapshot: dataSnapshot) , options: .prettyPrinted)
            let notes = try JSONDecoder().decode([Note].self, from: jsonData)
            self.notes = notes
            delegate?.didNoteUpdateNotified(result: .nonEmpty)
        } catch {
            print("Error: \(error)")
        }
    }
    
    func toJSon(dataSnapshot: DataSnapshot) -> [[String: Any]] {
        notes?.removeAll()
        var json: [[String:Any]] = []
        
        dataSnapshot.children.allObjects.forEach({ (snapshot) in
            let note = snapshot as! DataSnapshot
            json.append(note.value as! [String : Any])
        })
        
        return json
    }
    
    func sort(type: FilterType) {
        guard let _ = Auth.auth().currentUser?.uid else { return }
        switch type {
        case .date:
            notes = notes?.sorted(by: { $0.date > $1.date })
        case .priority:
            notes = notes?.sorted(by: { $0.priority.rawValue > $1.priority.rawValue })
        case .status:
            notes = notes?.sorted(by: { $0.isTaskCompleted.intValue > $1.isTaskCompleted.intValue })
        case .none:
            break
        }
        
        delegate?.didNoteUpdateNotified(result: .nonEmpty)
    }
}
