//
//  AccountViewModel.swift
//  MyMOM
//
//  Created by Meera Sayabu on 30/04/19.
//  Copyright © 2019 Meera Sayabu. All rights reserved.
//

import Foundation
import FirebaseAuth

protocol AccountViewable {
    var isUserLoggedIn: Bool { get }
    func signOut()
    var delegate: AccountViewModelDelegate? { set get }
    var userName: String? { get }
    var userEmail: String? { get }
}

protocol AccountViewModelDelegate: NSObjectProtocol {
    func logoutFailedWith(error: Error)
    func loggedOut()
}

class AccountViewModel: AccountViewable {
    weak var delegate: AccountViewModelDelegate?
    
    var isUserLoggedIn: Bool {
        return Auth.auth().currentUser?.refreshToken != nil
    }
    
    private var currentUser: User? {
        return Auth.auth().currentUser
    }
    
    var userName: String? {
        return currentUser?.displayName
    }
    
    var userEmail: String? {
        return currentUser?.email
    }
    
    init() {
        // Initialize dependencies
    }
    
    func signOut() {
        do {
            try Auth.auth().signOut()
            delegate?.loggedOut()
        } catch {
            delegate?.logoutFailedWith(error: error)
        }
    }
}
