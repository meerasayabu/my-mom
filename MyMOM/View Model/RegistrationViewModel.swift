//
//  RegistrationViewModel.swift
//  MyMOM
//
//  Created by Meera Sayabu on 27/04/19.
//  Copyright © 2019 Meera Sayabu. All rights reserved.
//

import Foundation
import FirebaseAuth
import Firebase

enum InvalidInputType {
    case email
    case password
    case name
    
    var errorMessage: String {
        switch self {
        case .email:
            return "Is seems like you have entered invalid email id"
        case .name:
            return "Name shoudl have atleast 3 character"
        case .password:
            return "Password must be more than 3 character and confirm password should be same with password"
        }
    }
}

enum ResultType {
    case success
    case failure
}

protocol RegistrationViewModelDelegate: NSObjectProtocol {
    func invalidInput(type:InvalidInputType)
    func registrationResult(resultType: ResultType, error: Error?)
}

protocol RegistrationViewable {
    var delegate: RegistrationViewModelDelegate? { set get }
    func isValidEmail(email: String?) -> Bool
    func isValidName(name: String?) -> Bool
    func isValidPassword(password: String?, confirmPassword: String?) -> Bool
    func registerUser(email: String?, password: String?, confirmPassword: String?, name: String?)
    func registrationFailedMessage(error: Error?) -> String
}

class RegistrationViewModel: RegistrationViewable {
    
    weak var delegate: RegistrationViewModelDelegate?
    
    init() {
        // Initialize dependencies
    }

    func isValidPassword(password: String?, confirmPassword: String?) -> Bool {
        guard let pwd = password, let confirmPwd = confirmPassword, pwd == confirmPwd , pwd.count > 3 else { return false }
        return true
    }
    
    func isValidName(name: String?) -> Bool {
        guard let name = name, name.count > 3 else { return false }
        return true
    }
    
    func isValidEmail(email: String?) -> Bool {
        guard let mail = email, mail.isValidEmail() else { return false }
        return true
    }
    
    func registerUser(email: String?, password: String?, confirmPassword: String?, name: String?) {
        // Vaidate name, minimum name length is 1
        guard isValidName(name: name)  else {
            delegate?.invalidInput(type: .name)
            return
        }
        // Vaidate email
        guard isValidEmail(email: email) else {
            delegate?.invalidInput(type: .email)
            return
        }
        // Vaidate password, minimum password length is 3
        guard isValidPassword(password: password, confirmPassword: confirmPassword)  else {
            delegate?.invalidInput(type: .password)
            return
        }
        
        Auth.auth().createUser(withEmail: email!, password: password!) {[weak self] (result, error) in
            guard let strongSelf = self else { return }
            if error == nil {
                let data = result?.user.createProfileChangeRequest()
                // Save user name in firebase user auth profile
                data?.displayName = name!
                data?.commitChanges(completion: { (commitError) in
                    if commitError == nil {
                        strongSelf.delegate?.registrationResult(resultType: .success, error: nil)
                    } else {
                        strongSelf.notify(error: commitError)
                    }
                })
            } else {
                strongSelf.notify(error: error)
            }
        }
    }
    
    private func notify(error: Error?) {
        // send error notification to controller
        delegate?.registrationResult(resultType: .failure, error: error)
    }
    
    func registrationFailedMessage(error: Error?) -> String {
        guard let err = error else { return "Registration failed." }
        return "Registration failed. Please check the error \(err.localizedDescription)"
    }
}
