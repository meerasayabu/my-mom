//
//  CreateNteViewModel.swift
//  MyMOM
//
//  Created by Meera Sayabu on 28/04/19.
//  Copyright © 2019 Meera Sayabu. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseAuth

protocol CreateViewModelDelegate: NSObjectProtocol {
    func didNotesSaved()
    func didSavingFailed(error: Error?)
}

protocol CreateNoteViewable {
    var delegate: CreateViewModelDelegate? { set get }
    var priority: NotePriority { set get }
    var isCompleted: Bool { set get }
    func saveNote(title: String, note: String, date: Double, isUpdate: Bool, noteKey: String?)
    func isValidateTextInFields(titleTextField: UITextField, dateTextField: UITextField, noteTextView: UITextView) -> (Bool, String?)
    var notes: [Note]? { get }
}

class CreateNoteViewModel: CreateNoteViewable {
    var isCompleted: Bool
    
    var notes: [Note]? {
        return homeViewModel.notes
    }
    
    var priority: NotePriority
    weak var delegate: CreateViewModelDelegate?
    private var databaseRef: DatabaseReference!
    private var homeViewModel: HomeViewable
    
    init(homeViewModel: HomeViewable) {
        databaseRef = Database.database().reference()
        priority = .undefined
        isCompleted = false
        self.homeViewModel = homeViewModel
    }
    
    func saveNote(title: String, note: String, date: Double, isUpdate: Bool, noteKey: String?) {
        guard let uid = Auth.auth().currentUser?.uid, let key = databaseRef.child("users/\(uid)/notes").childByAutoId().key else {
            return
        }
        
         let notePayload = ["title":title,
                     "name": Auth.auth().currentUser!.displayName!,
                     "date": date,
                     "priority": priority.rawValue,
                     "isTaskCompleted": isCompleted,
                     "note": note,
                     "key": isUpdate ? noteKey! : key]
            as [String : Any]
        if isUpdate {
            databaseRef.child("users/\(uid)/notes").child(noteKey!).updateChildValues(notePayload) {[weak self] (error, databaseReference) in
                if error == nil {
                    self?.delegate?.didNotesSaved()
                } else {
                    self?.delegate?.didSavingFailed(error: error)
                }
            }
        } else {
            databaseRef.child("users/\(uid)/notes").child(key).setValue(notePayload) {[weak self] (error, databaseReference) in
                if error == nil {
                    self?.delegate?.didNotesSaved()
                } else {
                    self?.delegate?.didSavingFailed(error: error)
                }
            }
        }
    }
    
    func isValidateTextInFields(titleTextField: UITextField, dateTextField: UITextField, noteTextView: UITextView) -> (Bool, String?) {
        guard let titleTxt = titleTextField.text, titleTxt.count > 5 else {
            return (false,"Title sould have more than 5 characters")
        }
        guard let _ = dateTextField.text else {
            return (false,"Date should not be empty")
        }
        guard let noteTxt = noteTextView.text, noteTxt.count > 5 else {
            return (false,"Title sould have more than 5 characters")
        }
        return (true,nil)
    }
    
    private func fetchNotes() {
        homeViewModel.notifyNotesChanges()
    }
}
