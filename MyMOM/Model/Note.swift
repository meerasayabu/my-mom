//
//  Note.swift
//  MyMOM
//
//  Created by Meera Sayabu on 27/04/19.
//  Copyright © 2019 Meera Sayabu. All rights reserved.
//

import Foundation

struct Note: Decodable {
    var title: String
    var date: Double
    var note: String
    var isTaskCompleted: Bool
    var priority: NotePriority
    var name: String
    var key: String
    
    enum NoteCodingKeys: String, CodingKey {
        case title
        case date
        case note
        case isTaskCompleted
        case priority
        case name
        case key
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: NoteCodingKeys.self)
        title = try container.decode(String.self, forKey: .title)
        date = try container.decode(Double.self, forKey: .date)
        note = try container.decode(String.self, forKey: .note)
        isTaskCompleted = try container.decode(Bool.self, forKey: .isTaskCompleted)
        priority = try container.decode(NotePriority.self, forKey: .priority)
        name = try container.decode(String.self, forKey: .name)
        key = try container.decode(String.self, forKey: .key)
    }
}

extension Note {
    func toDictionaty() -> [String: Any] {
        return [
            "title":title,
            "date":date,
            "notes":note,
            "isTaskCompleted":isTaskCompleted,
            "priority":priority.rawValue,
            "name":name,
            "key": key
        ]
    }
}

enum NotePriority: Int, Decodable {
    case low = 1
    case medium = 2
    case high = 3
    case undefined = 4
    
    var indentifier: String {
        switch self {
        case .high:
            return "high"
        case .low:
            return "low"
        case .medium:
            return "medium"
        default:
            return "undefined"
        }
    }
}
